#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";

DEFAULT_PREFIX=MicroservicesKit-
export KIT_PROJECT_PREFIX="${KIT_PROJECT_PREFIX:-${DEFAULT_PREFIX}}"

export TARGET_URL="${1}"

"${SCRIPT_DIR}"/GuestInfoAPI/deploy.sh "${TARGET_URL}" https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfoapi.git 85f7029d8c8176dd8c4920e20cd77f43f3a0702c
"${SCRIPT_DIR}"/GuestInfoBackend/deploy.sh "${TARGET_URL}" https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfobackend 2be129cca3dc2bb2b86454647c6300f10e23f371
"${SCRIPT_DIR}"/GuestInfoDocumentation/deploy.sh "${TARGET_URL}" https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/documentation 7eefa060c4918b11231883251038fb0ea534e60f
"${SCRIPT_DIR}"/GuestInfoFrontend/deploy.sh "${TARGET_URL}" https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfofrontend 263aa8a9668e250aa00511083f230153fe4ba33b
"${SCRIPT_DIR}"/GuestInfoGeneral/deploy.sh "${TARGET_URL}" https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/general 5181c93e0ef7e287fbda5c078b3118fc25d87eb1
"${SCRIPT_DIR}"/GuestInfoIntegration/deploy.sh "${TARGET_URL}" https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfointegration 9676d3a3ce639ce7e0b05d2d9134d1fc791bb266
"${SCRIPT_DIR}"/MicroservicesActivities/deploy.sh "${TARGET_URL}" https://gitlab.com/LibreFoodPantry/training/microservices/microserviceskit/microservices-activities 6cca9cfee296baa57da0c7b8b06d6dfc8e150444
