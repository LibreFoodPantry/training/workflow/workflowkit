# Instructor setup instructions

**This is currently a manual setup process. This will eventually be automated.**

1. Create group for each team. Used LibreFoodPantry/Training/WSU Spring 2023 Practice/Team N
2. Fork GuestInfoAPI to group, break fork relationship
3. Create 1 trivial issue for each member of the team:

    Title: `Remove GET /guests query parameter for resident`

    Description:

    ```text
    The path `GET /guests?resident=` should not exist in the API.

    There is no actual use case for this query. It was added just to have an example of a query parameter.

    Remove this optional parameter from the endpoint.
    ```

    Title: `Remove MongoID schema`

    Description:

    ```text
    The `MongoID` schema is left over from a previous version of the backend which returned the MongoID with every record. It is not used by any of the current endpoints.

    Remove the schema from the API.
    ```

    Title: `Make WSUID 8 digits long`

    Description:

    ```text
    Change the `WSUID` schema so that id numbers are 8 digits instead of 7. Don't forget to change the example as well.

    (This is not a real change that is needed by the customer. It is just for the workflow practice.)
    ```

    Title: `Add a zip+4 example`

    Description:

    ```text
    The regex for `ZipCode` allows zip+4. Add a second example to the `ZipCode` schema: `01602-2597`.
    ```

    Title: `Add version number example for beta version`

    Description:

    ```text
    The `APIVersion` regex allows more than just three numbers. Add an example to the `APIVersion` schema for `2.0.0-beta`.
    ```
